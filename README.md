# cypress-test-framework

This is a test framework based on cypress automation tool.

## Initial Steps

Ensure node and npm are installed. Download the git repo to you local. Run the below command to download all the required packages.

``npm install``

## Running tests

There are two ways to run the test.

1) cypress UI - Using the below command user is able to open the cypress gui and pick the tests that needs to be run. This would be usefull during the development and debugging activities.

``npx cypress open``

2) The next way is to run tests using command line. You can use the below command to run the tests in command mode. By default the command mode execution is run on headless mode on the electron browser

``npx cypress run``

Users can also run the tests using below command

``npm test``

Users can run the tests in other browsers which are compatible with cypress. As of now you can run it on Chome, Firefox and Edge which are all Chromium based browsers other than the default Electron browser. You the below command to run the tests on chrome.

``npx cypress run --browser chrome``

## Miscellaneous

The tests are currently in the integration/examples folder. User can try out the example tests available
